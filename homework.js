function tinhluong() {
  var so_ngay_cong = document.getElementById("so_ngay_cong").value * 1;

  var luong_1_ngay = document.getElementById("luong_1_ngay").value * 1;

  var tien_luong;

  tien_luong = luong_1_ngay * so_ngay_cong;

  tien_luong = tien_luong.toLocaleString("it-IT", {
    style: "currency",
    currency: "VND",
  });

  document.getElementById(
    "tien_luong"
  ).innerHTML = `<div>Tiền lương của bạn là ${tien_luong}</div>`;
}
function tinh_trung_binh() {
  var so_thu_nhat = document.getElementById("so_thu_nhat").value * 1;

  var so_thu_hai = document.getElementById("so_thu_hai").value * 1;

  var so_thu_ba = document.getElementById("so_thu_ba").value * 1;

  var so_thu_tu = document.getElementById("so_thu_tu").value * 1;

  var so_thu_nam = document.getElementById("so_thu_nam").value * 1;

  var trung_binh =
    (so_thu_nhat + so_thu_hai + so_thu_ba + so_thu_tu + so_thu_nam) / 5;
  document.getElementById(
    "trung_binh"
  ).innerHTML = `<div>Trung bình cộng của 5 số trên là: ${trung_binh}</div>`;
}

function quy_doi() {
  var gia_usd = document.getElementById("gia_usd").value * 1;

  var tien_usd = document.getElementById("tien_usd").value * 1;

  var tien_vnd = tien_usd * gia_usd;

  tien_vnd = tien_vnd.toLocaleString("it-IT", {
    style: "currency",
    currency: "VND",
  });

  document.getElementById(
    "tien_vnd"
  ).innerHTML = `<div>Số tiền được quy đổi là: ${tien_vnd}</div>`;
}
function tinh_dien_tich() {
  var chieu_rong = document.getElementById("chieu_rong").value * 1;

  var chieu_dai = document.getElementById("chieu_dai").value * 1;

  var dien_tich = chieu_dai * chieu_rong;

  var chu_vi = (chieu_dai + chieu_rong) * 2;

  document.getElementById(
    "dien_tich"
  ).innerHTML = `<div>Diện tích hình chữ nhật là: ${dien_tich}</div>`;
  document.getElementById(
    "chu_vi"
  ).innerHTML = `<div>Chu vi hình chữ nhật là: ${chu_vi}</div>`;
}

/** Bài 5
 * input: nhập number
 * lấy đơn vị: thực hiện phép chia lấy dư cho 10 (number%10)
 * lấy hàng chục: thực hiện phép chia lấy số nguyên cho 10, lấy number trừ đi số dư để chia 10 ra số nguyên
 */
function tinh_tong() {
  var number = document.getElementById("number").value * 1;

  var donvi = number % 10;

  var chuc = (number - (number % 10)) / 10;

  var tong_2_so = chuc + donvi;

  document.getElementById(
    "tong_ky_so"
  ).innerHTML = `Tổng 2 ký số là ${tong_2_so}</div>`;
}
